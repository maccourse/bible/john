---
title: "1 John Bible Study"
date: "2021-09-11"
draft: false
---

*...that which we have seen and heard we declare to you, that you also may have fellowship with us; and truly our fellowship is with the Father and with His Son Jesus Christ.* (1 John 1.3)

<!--more-->

This class is a study of the first epistle of St. John the Theologian, one of the Twelve Apostles, and author of the Gospel of John.  I am teaching the class at the request of Fr. Maximos Politis of [Holy Trinity Greek Orthodox Church](http://www.holytrinitygoc.com) in St. Augustine, Florida, with the blessing of my parish priest, Fr. Damian Kuolt of [St Jacob of Alaska OCA Church](https://orthodoxbend.com) in Bend, Oregon. You do not need to be a member of Holy Trinity or St Jacob's to participate in the Bible study.

We meet via Zoom on Thursdays at 8 PM Eastern, 5 PM Pacific.  I try to keep the duration of the class to 60 minutes, dividing the time between a lesson on a passage of Scripture, followed by questions, answers, and comments, then a short time for people to say how they are doing and share any prayer requests.  We then pray for one another.

Click on the "Online" menu option above to join the Zoom meeting. Contact me (Michael Colburn) for the passcode if you do not have it.

We are using the New King James Version (NKJV) translation as found in the Orthodox Study Bible. As needed, I will discuss other translations as well as the Greek text.

As with all Bible studies that I teach, the goal is:

- To behold in our hearts the glory of Christ
- To participate more deeply in life in the Spirit
- To become more conformed to His image
- To pray for and encourage one another

A video recording of each study is provided below.  Also, at the bottom, are books and articles that I have referred to in the class.

|Passage| Slides | Video 
|:------:|:---------:|:---------:|
|1 John 1.1-4| [Slides](/pdfs/studies/1john/1john.1.1-4.pdf) | [Video](https://youtu.be/L3Ya8q6JlzI)        |
|1 John 1.5-10| [Slides](/pdfs/studies/1john/1john.1.5-10.pdf) | [Video](https://youtu.be/Ia3AFNDE__4)        |
|1 John 2.1-6| [Slides](/pdfs/studies/1john/1john.2.1-6.pdf) | [Video](https://youtu.be/JbamhqqYshA)        |
|1 John 2.7-14| [Slides](/pdfs/studies/1john/1john.2.7-14.pdf) | [Video](https://youtu.be/HYfxd00v200)        |
|1 John 2.15-17| [Slides](/pdfs/studies/1john/1john.2.15-17.pdf) | [Video](https://youtu.be/0ZKwEazbtDI)        |
|1 John 2.18-27| [Slides](/pdfs/studies/1john/1john.2.18-27.pdf) | [Video](https://youtu.be/Jk5sCYgBq54)        |
|1 John 2.28-3.3| [Slides](/pdfs/studies/1john/1john.2.28-3.3.pdf) | [Video](https://youtu.be/IA7tV3jlo8w)        |
|1 John 3.4-12| [Slides](/pdfs/studies/1john/1john.3.4-12.pdf) | [Video](https://youtu.be/o_IOFiRKiKE)        |
|1 John 3.13-24| [Slides](/pdfs/studies/1john/1john.3.13-24.pdf) | [Video](https://youtu.be/7iPr3cqWrUY)        |
|1 John 4.1-6| [Slides](/pdfs/studies/1john/1John4.1-6.pdf) | [Video](https://youtu.be/SFZ99CA0LY0)        |
|1 John 4.7-21| [Slides](/pdfs/studies/1john/1john.4.7-21.pdf) | [Video](https://youtu.be/2WyVJ9CoyP0)        |
|1 John 5.1-12| [Slides](/pdfs/studies/1john/1john.5.1-12.pdf) | [Video](https://youtu.be/0RrDfC1ouSs)        |
|1 John 5.1-12| [Slides](/pdfs/studies/1john/1john.5.13-21.pdf) | [Video](https://youtu.be/oJkpCr2pKqs)        |

### Primary References

Farley, Fr. Lawrence. [Universal Truth: The Catholic Epistles of James, Peter, Jude, and John](https://www.amazon.com/Universal-Truth-Catholic-Epistles-Companion/dp/1888212608)

Zacharou, Fr. Zacharias. [The Hidden Man of the Heart](https://www.amazon.com/Hidden-Man-Heart-Peter-Anthropology-ebook/dp/B00ENWHAOU). An excerpt from chapter 1 is [here](/pdfs/articles/FrZachariasMysteryMansHeart.pdf).


