---
title: "Gospel of John Bible Study"
date: "2019-09-23"
draft: false
---

*The Good News that John wrote that you may believe that Jesus is the Christ, the Son of God, and that believing you may have life in his name.* (John 20.31)

---
The video of the Gospel of John is available on [Youtube](https://youtu.be/lchB_CEg5VI).

---
<!--more-->

The syllabus is provided below in the unit titled *Intro*. There are also PDFs of the slides, and in some cases a video recording of the study.  Also, at the bottom, are books that I have referred to in the class.

This class was taught in person as an adult Bible study on Sundays after the Divine Liturgy at [Holy Trinity Greek Orthodox Church](http://www.holytrinitygoc.com) in St. Augustine, Florida.  Due to the pandemic, we held the class via Zoom.  Videos are provided from that point on.

|Passage| Handouts | Video 
|:------:|:---------:|:---------:|
|Intro      |   [Syllabus](/pdfs/studies/john/syllabus.pdf)      |         |
|1.01-18|  [Slides](/pdfs/studies/john/study.john.01.01-18.pdf)       |         |
|1.19-28|  [Slides](/pdfs/studies/john/study.john.01.19-28.pdf)       |         |
|1.29-34|  [Slides](/pdfs/studies/john/study.john.01.29-34.pdf)       |         |
|1.35-51|  [Slides](/pdfs/studies/john/study.john.01.35-51.pdf)       |         |
|2.01-12|  [Slides](/pdfs/studies/john/study.john.02.01.12.pdf)       |         |
|2.13-25|  [Slides](/pdfs/studies/john/study.john.02.13.25.pdf)       |         |
|3.01-34|  [Slides](/pdfs/studies/john/study.john.03.01.34.pdf)       |         |
|4.01-42|  [Slides](/pdfs/studies/john/study.john.04.01.42.pdf)       |         |
|4.43-54|  [Slides](/pdfs/studies/john/study.john.04.43.54.pdf)       |         |
|5.01-15|  [Slides](/pdfs/studies/john/study.john.05.01.15.pdf)       |         |
|5.16-47|  [Slides](/pdfs/studies/john/study.john.05.16.47.topical.pdf)       |         |
|6.01-21|  [Slides](/pdfs/studies/john/study.john.06.1.21.pdf)       |         |
|6.22-40|  [Slides](/pdfs/studies/john/study.john.06.22.40.pdf)       |         |
|6.41-45|  [Slides](/pdfs/studies/john/study.john.06.41-45.pdf)       |         |
|6.46-71|  [Slides](/pdfs/studies/john/study.john.06.46-71.pdf)       | [Video](https://youtu.be/4TIkh5HkWTc)        |
|6.44 & 65|  [Slides](/pdfs/studies/john/study.john.06.44&65.pdf)      |  [Video](https://youtu.be/jKJ9mMjZPNw)        |
|Humility|  [Slides](/pdfs/studies/HumilityInTheLordsPrayer.pdf)      |  [Video](https://youtu.be/gTCEdCcC5uU)        |
|7.1-18|  [Slides](/pdfs/studies/john/study.john.07.01.18.pdf)      |  [Video](https://youtu.be/iGtDtv0B_sk)        |
|7.19-36|  [Slides](/pdfs/studies/john/study.john.07.19.36.pdf)      |  [Video](https://youtu.be/SgbW9G_OdEo)        |
|7.37-53|  [Slides](/pdfs/studies/john/study.john.07.37-53.pdf)      |  [Video](https://youtu.be/6wsHrK4ExLg)        |
|8.1-11|  [Slides](/pdfs/studies/john/study.john.08.02.11.pdf)      |  [Video](https://youtu.be/9cjSf_KCUzY)        |
|8.12 Part 1|  [Slides](/pdfs/studies/john/study.john.walkingInTheLightPart1.pdf)      |  [Video](https://youtu.be/i4apv4erp5o)        |
|8.12 Part 2|  [Slides](/pdfs/studies/john/study.john.walkingInTheLightPart2.pdf)      |  [Video](https://youtu.be/wkiH4tL2xtk)        |
|8.13-20|  [Slides](/pdfs/studies/john/study.john.08.13-20.pdf)      |  [Video](https://youtu.be/47ntDalSB-w)        |
|8.21-30|  [Slides](/pdfs/studies/john/study.john.08.21-30.pdf)      |  [Video](https://youtu.be/3hNlST1NgeY)        |
|8.31-36|  [Slides](/pdfs/studies/john/study.john.08.31-36.pdf)      |  [Video](https://youtu.be/cdOrokdKvMM)        |
|8.37-59|  [Slides](/pdfs/studies/john/study.john.08.37-59.pdf)      |  [Video](https://youtu.be/ndVnfDvVItA)        |
|9.01-41|  [Slides](/pdfs/studies/john/study.john.09.01-41.pdf)      |  [Video](https://youtu.be/ZwWhuArmtMc)        |
|10.01-21|  [Slides](/pdfs/studies/john/study.john.10.01-21.pdf)      |  [Video](https://youtu.be/jbejVkUSIrw)        |
|10.22-42|  [Slides](/pdfs/studies/john/study.john.10.22-42.pdf)      |  [Video](https://youtu.be/EbDdMIn8zl0)        |
|11.01-54|  [Slides](/pdfs/studies/john/study.john.11.01-54.pdf)      |  [Video](https://youtu.be/78WU8jVJR5Q)        |
|Life After Death|  [Slides](/pdfs/studies/john/study.life.after.death.pdf)      |  [Video](https://youtu.be/O_xRtKRgrHM)        |
|12.01-11|  [Slides](/pdfs/studies/john/study.john.12.01-11.pdf)      |  [Video](https://youtu.be/uQYUTHsWt_s)        |
|12.12-26|  [Slides](/pdfs/studies/john/study.john.12.12-36.pdf)      |  [Video](https://youtu.be/OC5Xi8H6zpI)        |
|12.27-36|  [Slides](/pdfs/studies/john/study.john.12.27-36.pdf)      |  [Video](https://youtu.be/zrbZdnb1kLU)        |
|12.37-50|  [Slides](/pdfs/studies/john/study.john.12.37-50.pdf)      |  [Video](https://youtu.be/oVSVMwbX1Tk)        |
|13.01-17|  [Slides](/pdfs/studies/john/study.john.13.01-17.pdf)      |  [Video](https://youtu.be/jj62OR6RSsM)        |
|13.18-30|  [Slides](/pdfs/studies/john/study.john.13.18-30.pdf)      |  [Video](https://youtu.be/td6p5U5W6CE)        |
|13.31-14.06|  [Slides](/pdfs/studies/john/study.john.13.31-14.06.pdf)      |  [Video](https://youtu.be/Y_F3DU9x8aI)        |
|14.07-17|  [Slides](/pdfs/studies/john/study.john.14.07-17.pdf)      |  [Video](https://youtu.be/FWB4CuOOPlI)        |
|14.18-31|  [Slides](/pdfs/studies/john/study.john.14.18-31.pdf)      |  [Video](https://youtu.be/Igw5Tr2oHOM)        |
|15.01-17|  [Slides](/pdfs/studies/john/study.john.15.01-17.pdf)      |  [Video](https://youtu.be/9FrKbqWLcTM)        |
|15.18-27|  [Slides](/pdfs/studies/john/study.john.15.18-27.pdf)      |  [Video](https://youtu.be/O5hwcRsj7yU)        |
|16.01-15|  [Slides](/pdfs/studies/john/study.john.16.01-15.pdf)      |  [Video](https://youtu.be/Y7su8emjZ6E)        |
|16.16-33|  [Slides](/pdfs/studies/john/study.john.16.16-33.pdf)      |  [Video](https://youtu.be/r69WczT7A2c)        |
|Theophany| [Slides](/pdfs/studies/TheophanyShort.pdf)                |  [Video](https://youtu.be/JS_ndLiG-Yw)        |
|17.1-8| [Slides](/pdfs/studies/john/study.john.17.1-8.pdf)                |  [Video](https://youtu.be/ghzTrD6nQ44)        |
|17.9-19| [Slides](/pdfs/studies/john/study.john.17.9-19.pdf)                |  [Video](https://youtu.be/ghzTrD6nQ44)        |
|17.20-26| [Slides](/pdfs/studies/john/study.john.17.20-26.pdf)                |  [Video](https://youtu.be/ULXJZlDS70M)        |
|18.1-11| [Slides](/pdfs/studies/john/study.john.18.1-11.pdf)                |  [Video](https://youtu.be/Kv7ztc-lLws)        |
|18.12-27| [Slides](/pdfs/studies/john/study.john.18.12-27.pdf)                |  [Video](https://youtu.be/fNIpYgKiIyA)        |
|18.28-40| [Slides](/pdfs/studies/john/study.john.18.28-40.pdf)                |  [Video](https://youtu.be/6_xArlAeJYg)        |
|19.01-37| [Slides](/pdfs/studies/john/study.john.19.01-37.pdf)                |  [Video](https://youtu.be/JPdJPIdyalg)        |
|19.38-20.18| [Slides](/pdfs/studies/john/study.john.19.38-20.18.pdf)                |  [Video](https://youtu.be/XbdmnFp9d1U)        |
|20.19-31| [Slides](/pdfs/studies/john/study.john.20.19-31.pdf)                |  [Video](https://youtu.be/OlMuY8NmXRk)        |
|21.01-25| [Slides](/pdfs/studies/john/study.john.21.01-25.pdf)                |  [Video](https://youtu.be/ML-jmWsjdSo)        |
|Salvation - Part 1| [Slides](/pdfs/studies/salvation/salvation.part1.pdf)                |         |
|Salvation - Introduction|                 |  [Video](https://youtu.be/1I35x4GB7OQ)        |
|Salvation - Model 1 - Christ the Teacher|                 |  [Video](https://youtu.be/TRUrnIBLvRc)        |
|Salvation - Model 5 - Christ the Example|                 |  [Video](https://youtu.be/eDo03D9l3As)        |
|Salvation - Part 2| [Slides](/pdfs/studies/salvation/salvation.part2.pdf)                |         |
|Salvation - Model 2 - Christ the Ransom|                 |  [Video](https://youtu.be/Vw9t3tVF_ec)        |
|Salvation - Model 3 - Christ the Sacrifice|                 |  [Video](https://youtu.be/RUjwoIydAH8)        |
|Salvation - Part 3| [Slides](/pdfs/studies/salvation/salvation.part3.pdf) | [Video](https://youtu.be/uvt--JtPXTs)         |
|Salvation - Model 4 - Christ the Victor|                 |  [Video](https://youtu.be/DYqi04RqDXM)        |
|Salvation - Model 6 - Christ the Exchange|                 |  [Video](https://youtu.be/7GAJk9bNG28)        |
|Salvation - Model 7 - We With Christ| [Slides](/pdfs/studies/salvation/salvation.part4.pdf)                 |  [Video](https://youtu.be/j5vs3cxRNWA)        |


### Primary References

Farley, Fr. Lawrence. [The Gospel of John: Beholding the Glory](https://www.amazon.com/Gospel-John-Beholding-Orthodox-Companion/dp/1888212551)

Papadopoulos, Bishop Gerasimos. [The Gospel of St. John: A Commentary](https://www.amazon.com/Gospel-St-John-Commentary/dp/1935317148)

### Patristic References

St. Cyril of Alexandria. Commentary on John [Book 9](http://www.tertullian.org/fathers/cyril_on_john_09_book9.htm)

St. Isaac of Syria. [The Ascetical Homilies](https://www.amazon.com/Ascetical-Homilies-St-Isaac-Syrian/dp/B0062BZ1EG/)

St. John Chrysostom. [Homilies on the Gospel of Matthew](https://www.newadvent.org/fathers/2001.htm)

St. John Chrysostom. [Homilies on the Gospel of John](https://www.newadvent.org/fathers/2401.htm)

St. Symeon the New Theologian. [On the Mystical Life: the Ethical Discourses, Vol. 1](https://www.amazon.com/Mystical-Life-Ethical-Discourses-Church/dp/0881411426).

### Supplemental References

Aden, Basil Ross. [Justification and Sanctification - A Conversation between Lutheranism and Orthodoxy](/pdfs/studies/salvation/JustificationandSanctification.pdf).

Cabasilas, Nicholas. [The Life in Christ](https://www.amazon.com/Life-Christ-English-Ancient-Greek/dp/0913836125).

Metropolitan Kallistos Ware. [How Are We Saved? The Understanding of Salvation in the Orthodox Tradition](https://www.amazon.com/How-Are-Saved-Understanding-Salvation/dp/1880971224).

Moloney, Francis J., SDB. [Sacra Pagina: The Gospel of John](https://www.amazon.com/Sacra-Pagina-Francis-J-Moloney-ebook/dp/B01HO65XWM).

Pomazansky, Fr. Michael. [Orthodox Dogmatic Theology](https://www.amazon.com/Orthodox-Dogmatic-Theology-Concise-Exposition/dp/0938635697). 

Schmemann, Fr. Alexander. 2003. [The Eucharist: Sacrament of the Kingdom](https://www.amazon.com/Eucharist-Sacrament-Kingdom-Alexander-Schmemann/dp/0881410187).

Schmemann, Fr. Alexander. 2003. [O Death, Where is Thy Sting?](https://www.amazon.com/Death-Where-Thy-Sting/dp/0881412384).

Schmemann, Fr. Alexander. 2017. [The Liturgy of Death](https://www.amazon.com/Liturgy-Death-Alexander-Schmemann/dp/0881415561).

Staniloae, Fr. Dumitru. 2012. [The Experience of God, Vol. 4: The Church, Communion in the Holy Spirit](https://www.amazon.com/Experience-God-Church-Communion-Spirit/dp/1935317261).

Staniloae, Fr. Dumitru. 2013. [The Experience of God, Vol. 6: The Fulfillment of Creation](https://www.amazon.com/Experience-God-vol-Fulfillment-Creation/dp/1935317342). Note: after the questions/comment slide in my presentation titled "Life After Death", I have quotations from this book.

Vlachos, Metropolitan Hierotheos. 1992. [Orthodox Spirituality- a Brief Introduction](https://www.amazon.com/Orthodox-Spirituality-Metropolitan-Nafpaktos-Hierotheos/dp/9607070208).  This is a translation of the original Greek version: ΜΙΚΡΑ ΕΙΣΟΔΟΣ στήν Ὀρθόδοξη Πνευματικότητα. ῎Εκδοση Β´ 1998. ΑΠΟΣΤΟΛΙΚΗ ΔΙΑΚΟΝΙΑ.

Vlachos, Metropolitan Hierotheos. 2015. [Life after Death](https://www.amazon.com/after-Death-Metropolitan-Nafpaktos-Hierotheos/dp/9607070348). This is a translation of the original Greek version: Ἡ ζωή μετά τόν θάνατο, ἐκδόσεις Γ 2006. Other Greek editions were subsequently published.

### Videos

The following three videos were broken into seven for presentation in our class:

[Metropolitan Kallistos Ware - Salvation in Christ - Part 1](https://youtu.be/XWuu215jPh0)

[Metropolitan Kallistos Ware - Salvation in Christ - Part 2](https://youtu.be/mi7BxVCFxvo)

[Metropolitan Kallistos Ware - Salvation in Christ - Part 3](https://youtu.be/GhOwjChoclc)
