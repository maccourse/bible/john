---
title: "Hebrews Bible Study"
date: "2022-01-31"
draft: false
---

*Let us hold fast the confession of our hope without wavering, for He who promised is faithful. And let us consider one another in order to stir up love and good works, not forsaking the assembling of ourselves together, as is the manner of some, but exhorting one another, and so much the more as you see the Day approaching.* (Hebrews 10:23-24)

<!--more-->

This class is a study of the Book of Hebrews. I am teaching at the request of Fr. Maximos Politis of [Holy Trinity Greek Orthodox Church](http://www.holytrinitygoc.com) in St. Augustine, Florida, with the blessing of my parish priest, Fr. Damian Kuolt of [St Jacob of Alaska OCA Church](https://orthodoxbend.org) in Bend, Oregon. You do not need to be a member of Holy Trinity or St Jacob's to participate in the Bible study.

We meet via Zoom on Mondays at 8 PM Eastern, 5 PM Pacific. I repeat the class at 8 PM Pacific. I try to keep the duration of the class to 60 minutes, dividing the time between a lesson on a passage of Scripture, followed by questions, answers, and comments, then a short time for people to say how they are doing and share any prayer requests.  We then pray for one another.  The video recording only has the teaching part.

Click on the "Online" menu option above to join the Zoom meeting. Contact me (Michael Colburn) for the passcode if you do not have it.

We are using the New King James Version (NKJV) translation as found in the Orthodox Study Bible. As needed, I will discuss other translations as well as the Greek text.

As with all Bible studies that I teach, the goal is:

- To behold in our hearts the glory of Christ
- To participate more deeply in life in the Spirit
- To become more conformed to His image
- To pray for and encourage one another

A video recording of each study is provided below.  Also, at the bottom, are books and articles that I have referred to in the class.

|Passage| Slides | Video | Comments |
|:------:|:---------:|:---------:|:---------:|
|Hebrews Intro (Full)| [Slides](/pdfs/studies/hebrews/heb.0.0-intro.pdf) | [Video](https://youtu.be/9kRdUF2ZcLU)        |This is the hour long introduction on Hebrews I gave at the 5 PM class.  |
|Hebrews Intro (Short)| [Slides](/pdfs/studies/hebrews/heb.0.0-intro-short.pdf) | [Video](https://youtu.be/rQBF-s5wQUw)        | This is the shortened, 30 minute, introduction I gave for the 8 PM class.   |

More references will be added over the coming weeks. Note that the only Orthodox commentary I know about on the book of Hebrews is the one by Fr. Lawrence Farley (see below).  If you want to buy a commentary for the class, I recommend his.  St. John Chrysostom preached a series of homilies on Hebrews.  They are available online, and I have provided the link in the Patristic references below.

### Primary References

Bruce, F. F. [The Epistle to the Hebrews](https://www.amazon.com/Epistle-Hebrews-International-Commentary-Testament/dp/0802824927) 

Farley, Fr. Lawrence. [The Epistle to the Hebrews](https://www.amazon.com/Epistle-Hebrews-High-Priest-Heaven/dp/1936270749/)

Hagnar, Donald A. [Encountering the Book of Hebrews](https://www.amazon.com/Encountering-Book-Hebrews-Exposition-Biblical/dp/080102580X)

Healey, Mary. [Hebrews (Catholic Commentary on Sacred Scripture)](https://www.amazon.com/Hebrews-Catholic-Commentary-Sacred-Scripture/dp/0801036038)

Heen, Krey, and Oden, editors. [Hebrews (Ancient Christian Commentary on Scripture, NT Volume 10)](https://www.amazon.com/Hebrews-Ancient-Christian-Commentary-Scripture/dp/0830814957)

Malaty, Fr.Tadros Yacoub. [The Epistle to the Hebrews](/pdfs/studies/hebrews/TheEpistleToTheHebrews.FatherTadrosYacoubMalaty.pdf).  Fr. Tadros is a Coptic priest.  This is a link to the PDF of his commentary.

Murray, Andrew. [Holiest of All: A Commentary on the Book of Hebrews](https://www.amazon.com/Holiest-All-Commentary-Book-Hebrews/dp/162911779X)

Wenham and Carson, editors. [New Bible Commentary : 21st Century Edition](https://www.amazon.com/New-Bible-commentary-21st-century/dp/085110648X)

### Apostolic and Patristic References

Chrysostom, John +407. [Homilies on Hebrews](https://ccel.org/ccel/schaff/npnf114/npnf114.v.i.html)

Clement of Rome +99. [1 Clement to the Corinthians](https://www.newadvent.org/fathers/1010.htm)

Eusebius of Caesarea +339. [Ecclesiastical History, Chapter 6](https://www.newadvent.org/fathers/250106.htm) Note: in chapter 6, he talks about the Book of Hebrews.

### Articles on the Literary Style of the Book of Hebrews

Many commentaries on the Book of Hebrews note that the author was highly educated both in Hellenic rhetoric and in rabbinic methods of exegesis and argumentation.  The article(s) below provide information in support of these claims.
 
Beauchamp, Jules. [Alliteration (Heb 1:1)](https://handleonhebrews.wordpress.com/2018/07/10/alliteration-heb-11/).  Note this is a blog, not a published journal article.

Witherington, Ben. [The Rhetorical Character of Hebrews](http://benwitherington.blogspot.com/2008/11/rhetorical-character-of-hebrews.html).  A lecture by a professor to the Society of Biblical Literature.

